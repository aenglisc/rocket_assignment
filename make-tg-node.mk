USER = "$(shell id -u):$(shell id -g)"

tg-node:
	docker-compose up tg_node

tg-node-build:
	docker-compose build tg_node

tg-node-install:
	docker-compose run tg_node make install

tg-node-setup: tg-node-build tg-node-install
