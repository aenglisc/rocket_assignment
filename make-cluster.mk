USER = "$(shell id -u):$(shell id -g)"

cluster:
	docker-compose up

cluster-setup: tg-node-setup user-node-setup
