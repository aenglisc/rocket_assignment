use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :user_node, queue_bot:  "test_bot_listener"
config :user_node, queue_user: "test_user_listener"

config :user_node, UserNodeWeb.Endpoint,
  http: [port: 4001],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :user_node, UserNode.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "user_node_test",
  hostname: "db",
  pool: Ecto.Adapters.SQL.Sandbox
