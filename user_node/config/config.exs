# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :user_node,
  ecto_repos: [UserNode.Repo]

# Configures the endpoint
config :user_node, UserNodeWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "6WYmfwGXg7g+KsH7s0RtjnoAh1hZrHYKOjAJbXPuyQxL3xkYTrnk6L8fSqWybMh2",
  render_errors: [view: UserNodeWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: UserNode.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
