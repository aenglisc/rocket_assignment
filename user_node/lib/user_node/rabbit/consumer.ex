defmodule UserNode.Rabbit.Consumer do
  require Logger
  use GenServer
  use AMQP
  alias UserNode.{
    Rabbit.Params,
    Texting,
  }
  alias UserNodeWeb.{
    SocketChannel,
  }

  @me __MODULE__
  @params %Params{}
  @queue "user_listener"
  @exchange "amq.direct"

  def start_link(_), do: GenServer.start_link(@me, @params, name: @me)

  def init(params = %Params{}) do
    {:ok, channel} = rabbitmq_connect(params)
    {:ok, %{channel: channel, params: params}}
  end

  def handle_info({:basic_consume_ok, %{consumer_tag: _}}, state), do: {:noreply, state}
  def handle_info({:basic_cancel_ok,  %{consumer_tag: _}}, state), do: {:noreply, state}
  def handle_info({:basic_cancel, %{consumer_tag: _}}, state), do: {:stop, :normal, state}

  def handle_info({:basic_deliver, payload, data}, state) do
    spawn(fn ->
      consume(state.channel,
              data.delivery_tag,
              data.redelivered,
              payload)
    end)
    {:noreply, state}
  end

  def handle_info({:DOWN, _, :process, _pid, _reason}, state) do
    {:ok, channel} = rabbitmq_connect(state.params)
    {:noreply, %{state | channel: channel}}
  end

  defp rabbitmq_connect(params = %Params{}) do
    maybe_conn = Connection.open(
      host: params.host,
      port: params.port,
      user: params.user,
      pass: params.pass)

    case maybe_conn do
      {:ok, conn} ->
        Logger.info("Connected to user_listener queue")
        Process.monitor(conn.pid)

        {:ok, channel} = Channel.open(conn)
        Queue.declare(channel, @queue, durable: true)
        Exchange.direct(channel, @exchange,  durable: true)
        Queue.bind(channel, @queue, @exchange)
        Basic.qos(channel, prefetch_count: 10)
        {:ok, _consumer_tag} = Basic.consume(channel, @queue)
        {:ok, channel}

      {:error, reason} ->
        Logger.warn("Unable to connect to rabbit: #{inspect reason}")
        Logger.warn("Reconnecting...")
        :timer.sleep(5_000)
        rabbitmq_connect(params)
    end
  end

  defp consume(channel, tag, redelivered, payload) do
    {:ok, msg} = Jason.decode(payload)
    case msg do
      %{"content" => content, "author" => "bot"} ->
        Texting.create_message(%{content: content, author: "bot"})
        SocketChannel.new(msg)
        Basic.ack channel, tag

      msg ->
        Basic.ack channel, tag
        Logger.warn("Unhandled message: #{inspect msg}")
    end
  rescue
    error ->
      Logger.error("MQ error: #{payload}; error: #{inspect error}")
      redeliver_after(channel, tag, redelivered, 5_000)
  end

  defp redeliver_after(channel, tag, redelivered, timeout) do
    :timer.sleep(timeout)
    Basic.reject channel, tag, requeue: not redelivered
  end
end
