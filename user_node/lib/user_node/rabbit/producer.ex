defmodule UserNode.Rabbit.Producer do
  require Logger
  use GenServer
  import UserNode.Texting
  alias UserNode.Rabbit.Params
  alias UserNodeWeb.{
    SocketChannel,
  }
  alias AMQP.{
    Basic,
    Channel,
    Connection,
    Queue,
  }

  @me __MODULE__
  @params %Params{}
  @queue "bot_listener"

  def start_link(_), do: GenServer.start_link(@me, @params, name: @me)
  def init(params = %Params{}), do: {:ok, params}

  def handle_cast({:send, message}, params) do
    send_message(message, params)
    {:noreply, params}
  end

  defp send_message(msg = %{"content" => content, "author" => author}, params) do
    with {:ok, _msg}    <- create_message(%{content: content, author: author}),
         {:ok, conn}    <- amqp_connection(params),
         {:ok, channel} <- Channel.open(conn),
         {:ok, message} <- Jason.encode(msg),
         {:ok, _res}    <- Queue.declare(channel, @queue, durable: true),
         :ok            <- Basic.publish(channel, "", @queue, message),
         :ok            <- Connection.close(conn)
    do
      SocketChannel.new(msg)
      Logger.info("A message has been sent to #{inspect @queue}")
      :ok
    else
      error ->
        Logger.error("Unable to send the message #{inspect error}")
        :error
    end
  end

  defp amqp_connection(params = %Params{}) do
    Connection.open(
      host: params.host,
      port: params.port,
      user: params.user,
      pass: params.pass)
  end
end
