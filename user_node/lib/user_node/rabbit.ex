defmodule UserNode.Rabbit do
  alias UserNode.Rabbit.Producer

  def send(msg), do: GenServer.cast Producer, {:send, msg}
end
