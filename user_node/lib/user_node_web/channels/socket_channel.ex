defmodule UserNodeWeb.SocketChannel do
  use UserNodeWeb, :channel
  alias UserNode.Rabbit
  alias UserNodeWeb.Endpoint

  def join("socket:rocket", _payload, socket) do
    {:ok, socket}
  end

  def new(message) do
    Endpoint.broadcast!("socket:rocket", "new_message", message)
  end

  def handle_in("message:add", msg = %{"content" => _, "author" => _}, socket) do
    Rabbit.send(msg)
    {:reply, :ok, socket}
  end
end
