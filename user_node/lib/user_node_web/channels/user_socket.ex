defmodule UserNodeWeb.UserSocket do
  use Phoenix.Socket

  ## Channels
  channel "socket:*", UserNodeWeb.SocketChannel

  ## Transports
  transport :websocket, Phoenix.Transports.WebSocket

  def connect(_params, socket) do
    {:ok, socket}
  end

  def id(_socket), do: nil
end
