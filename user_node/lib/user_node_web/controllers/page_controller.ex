defmodule UserNodeWeb.PageController do
  use UserNodeWeb, :controller
  import UserNode.Texting

  def index(conn, _params) do
    render conn, "index.html", messages: list_messages()
  end
end
