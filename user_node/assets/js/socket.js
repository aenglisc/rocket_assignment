import { Socket } from 'phoenix';

const renderMessage = (message) => {
  const messageTemplate = `
    <li class='list-group-item'>
    <b>${message.author}:</b>
    ${message.content}</li>
  `;
  document
    .querySelector('#messages')
    .insertAdjacentHTML('afterbegin', messageTemplate);
};

const socket = new Socket('/socket', { params: { token: window.userToken } });

socket.connect();

const channel = socket.channel('socket:rocket', {});

channel
  .join()
  .receive('ok', (resp) => { console.log('Joined successfully', resp); })
  .receive('error', (resp) => { console.log('Unable to join', resp); });

document
  .querySelector('#new-message')
  .addEventListener('submit', (e) => {
    e.preventDefault();
    const messageInput = e
      .target
      .querySelector('#message-content');

    if (messageInput.value !== '') {
      channel.push('message:add', {
        content: messageInput.value,
        author: 'user',
      });

      messageInput.value = '';
    }
  });

channel
  .on('new_message', (message) => {
    console.log('message', message);
    renderMessage(message);
  });
export default socket;
