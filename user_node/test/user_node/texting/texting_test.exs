defmodule UserNode.TextingTest do
  use UserNode.DataCase

  alias UserNode.Texting

  describe "messages" do
    alias UserNode.Texting.Message

    @valid_attrs %{author: "some author", content: "some content"}
    @update_attrs %{author: "some updated author", content: "some updated content"}
    @invalid_attrs %{author: nil, content: nil}

    def message_fixture(attrs \\ %{}) do
      {:ok, message} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Texting.create_message()

      message
    end

    test "list_messages/0 returns all messages" do
      message = message_fixture()
      assert Texting.list_messages() == [message]
    end

    test "get_message!/1 returns the message with given id" do
      message = message_fixture()
      assert Texting.get_message!(message.id) == message
    end

    test "create_message/1 with valid data creates a message" do
      assert {:ok, %Message{} = message} = Texting.create_message(@valid_attrs)
      assert message.author == "some author"
      assert message.content == "some content"
    end

    test "create_message/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Texting.create_message(@invalid_attrs)
    end

    test "update_message/2 with valid data updates the message" do
      message = message_fixture()
      assert {:ok, message} = Texting.update_message(message, @update_attrs)
      assert %Message{} = message
      assert message.author == "some updated author"
      assert message.content == "some updated content"
    end

    test "update_message/2 with invalid data returns error changeset" do
      message = message_fixture()
      assert {:error, %Ecto.Changeset{}} = Texting.update_message(message, @invalid_attrs)
      assert message == Texting.get_message!(message.id)
    end

    test "delete_message/1 deletes the message" do
      message = message_fixture()
      assert {:ok, %Message{}} = Texting.delete_message(message)
      assert_raise Ecto.NoResultsError, fn -> Texting.get_message!(message.id) end
    end

    test "change_message/1 returns a message changeset" do
      message = message_fixture()
      assert %Ecto.Changeset{} = Texting.change_message(message)
    end
  end
end
