defmodule UserNodeWeb.SocketChannelTest do
  use UserNodeWeb.ChannelCase

  alias UserNodeWeb.SocketChannel

  setup do
    {:ok, _, socket} =
      "user_id"
      |> socket(%{some: :assign})
      |> subscribe_and_join(SocketChannel, "socket:rocket")

    {:ok, socket: socket}
  end

  test "input messages are handled", %{socket: socket} do
    push socket, "message:add", %{"content" => "hi", "author" => "test"}
    assert_broadcast "new_message", %{"content" => "hi"}
  end

  test "incoming messages are handled", _socket do
    SocketChannel.new %{"content" => "hey", "author" => "test"}
    Process.sleep 100
    assert_broadcast "new_message", %{"content" => "hey"}
  end
end
