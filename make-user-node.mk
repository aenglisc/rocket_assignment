USER = "$(shell id -u):$(shell id -g)"

user-node:
	docker-compose up user_node

user-node-build:
	docker-compose build user_node

user-node-install:
	docker-compose run user_node make install

user-node-db-prepare:
	docker-compose run user_node make db-prepare

user-node-db-drop:
	docker-compose run user_node mix ecto.drop

user-node-setup: user-node-build user-node-install user-node-db-prepare

user-node-db-reset: user-node-db-drop user-node-db-prepare
