defmodule TgNode.Rabbit do
  alias TgNode.Rabbit.Producer

  def send(msg), do: GenServer.cast Producer, {:send, msg}
end
