defmodule TgNode.Bot.Polling.Server do
  use GenServer
  alias TgNode.Bot.Polling.Logic

  @me __MODULE__

  def start_link(_), do: GenServer.start_link(@me, 0, name: @me)

  def init(offset) do
    schedule_polling()

    {:ok, offset}
  end

  def handle_info(:poll, offset) do
    new_offset = Logic.poll(offset)
    schedule_polling()

    {:noreply, new_offset + 1}
  end

  defp schedule_polling, do: Process.send_after(self(), :poll, 1000)
end
