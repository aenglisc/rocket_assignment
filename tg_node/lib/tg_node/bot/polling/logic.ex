defmodule TgNode.Bot.Polling.Logic do
  alias TgNode.Bot.Config
  alias TgNode.Bot.Bot.Server, as: ReplyServer
  alias Nadia.Model.Update
  @id Application.get_env(:tg_node, :chat)

  def poll(offset) do
    offset
    |> updates
    |> send_updates
  end

  defp updates(offset) do
    try do
      case Nadia.get_updates([offset: offset]) do
        {:ok, new_updates} -> new_updates
        _                  -> []
      end
    catch
      []
    end
  end

  defp send_updates(updates), do: Enum.reduce(updates, 0, &handle_update/2)

  defp handle_update(%{update_id: id, channel_post: %{chat: %{id: @id}, text: text}}, _)
  when is_binary(text) do
    TgNode.Rabbit.send %{"content" => text, "author" => "bot"}
    id
  end

  defp handle_update(%{update_id: id}, _acc), do: id
end
