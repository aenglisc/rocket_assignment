defmodule TgNode.Rabbit.Producer do
  require Logger
  use GenServer
  alias TgNode.Rabbit.Params
  alias AMQP.{
    Basic,
    Channel,
    Connection,
    Queue,
  }

  @me __MODULE__
  @params %Params{}
  @queue "user_listener"

  def start_link(_), do: GenServer.start_link(@me, @params, name: @me)
  def init(params = %Params{}), do: {:ok, params}

  def handle_cast({:send, message}, params) do
    send_message(message, params)
    {:noreply, params}
  end

  defp send_message(msg, params) do
    with {:ok, conn}    <- amqp_connection(params),
         {:ok, channel} <- Channel.open(conn),
         {:ok, message} <- Jason.encode(msg),
         {:ok, _res}    <- Queue.declare(channel, @queue, durable: true),
         :ok            <- Basic.publish(channel, "", @queue, message),
         :ok            <- Connection.close(conn)
    do
      Logger.info("A message has been sent to #{@queue}")
      :ok
    else
      error ->
        Logger.error("Unable to send the message #{inspect error}")
        :error
    end
  end

  defp amqp_connection(params = %Params{}) do
    Connection.open(
      host: params.host,
      port: params.port,
      user: params.user,
      pass: params.pass)
  end
end
