defmodule TgNode do
  @moduledoc """
  Documentation for TgNode.
  """

  @doc """
  Hello world.

  ## Examples

      iex> TgNode.hello
      :world

  """
  def hello do
    :world
  end
end
