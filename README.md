# Rocketbank test assignment
You might need a VPN :D

Requirements:
* docker
* docker-compose
* make

Installation:
* clone
* cd rocket_assignment
* make cluster-setup

Run:
* make cluster
* go to [localhost:4001](localhost:4001)

TODO:
* proper tests
